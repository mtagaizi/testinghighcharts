var chart_;
var myData = [
  {
    batch: "B0",
    value: 2.3124044515428785,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B1",
    value: 2.307450153321662,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B2",
    value: 2.8121797928566608,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B3",
    value: 3.2646502397130033,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B4",
    value: 3.1285594520890285,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B5",
    value: 2.637527463792551,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B6",
    value: 2.8954048635993486,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B7",
    value: 3.1270723343422873,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B8",
    value: 3.129091166654628,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B9",
    value: 2.1820253949228947,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B10",
    value: 2.500223153594577,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B11",
    value: 2.5429046778296525,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B12",
    value: 2.24229885336481,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B13",
    value: 2.577406761420675,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B14",
    value: 2.730528400653886,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B15",
    value: 2.5108118114823412,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B16",
    value: 2.4475241096768667,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B17",
    value: 3.255895745992632,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B18",
    value: 2.296037375963877,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B19",
    value: 2.8211104310538007,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B20",
    value: 3.0878629282459364,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B21",
    value: 2.350392221292684,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B22",
    value: 3.2613209816175974,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B23",
    value: 3.0959627278219637,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B24",
    value: 2.135326644203686,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B25",
    value: 2.667028379641176,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B26",
    value: 2.770405474574022,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B27",
    value: 2.3254717513480005,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B28",
    value: 2.114328394124549,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B29",
    value: 2.636714065862651,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B30",
    value: 2.544690540256218,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B31",
    value: 3.0322069524772126,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B32",
    value: 3.2613713888981692,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B33",
    value: 2.318050306810325,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B34",
    value: 3.0437931150589894,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B35",
    value: 2.5683306479244843,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B36",
    value: 2.8779532041118028,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B37",
    value: 2.607764577661857,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B38",
    value: 2.778099825075394,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B39",
    value: 2.4744565057200605,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B40",
    value: 2.405885927773551,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B41",
    value: 2.1734682089055903,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B42",
    value: 2.8700260043099965,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B43",
    value: 2.408927237645905,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B44",
    value: 2.2279562467322696,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B45",
    value: 2.1771747053779102,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B46",
    value: 2.6297662464404765,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B47",
    value: 2.859447063939313,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B48",
    value: 3.128897528612942,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B49",
    value: 2.767725701103901,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B50",
    value: 2.1258033248860726,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B51",
    value: 3.220894917615861,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B52",
    value: 3.0115738620427117,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B53",
    value: 2.9326461714365486,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B54",
    value: 2.5364387773505337,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B55",
    value: 2.4955928881845098,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B56",
    value: 2.176900305465179,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B57",
    value: 2.511795627244527,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B58",
    value: 2.890391626472842,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B59",
    value: 3.231874616928903,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B60",
    value: 2.9212142893248725,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B61",
    value: 2.981293470933973,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B62",
    value: 2.3824620988831704,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B63",
    value: 2.91060106866692,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B64",
    value: 2.106366202896344,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B65",
    value: 2.7231385268945356,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B66",
    value: 3.2096548190639322,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B67",
    value: 3.0245745674192976,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B68",
    value: 2.6124321368545673,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  },
  {
    batch: "B69",
    value: 2.81033552879414,
    lower_alert_limit: 2.12,
    upper_alert_limit: 3.2,
    lower_warning_limit: 2.15,
    upper_warning_limit: 3.3,
    lower_specification_limit: 2,
    upper_specification_limit: 3.4
  }
];
var sum = 0;
myData.forEach(el => {
  sum += el.value;
});
var myRandomData = [...Array(300)].map(function(el, index) {
  return Math.random() * 10 | 0;
})
console.log('--------->', myRandomData.length);
var mean = sum / myData.length;
document.addEventListener("DOMContentLoaded", function() {
  var cccc = Highcharts.chart("container2", {
    chart: {
      type: "line",
      panning: true,
      zoomType: "xy",
      panKey: "shift"
    },
    title: {
      text: "FR77354234006347"
    },
    xAxis: {
      categories: myData.map(function(el) {
        return el.batch;
      })
    },
    yAxis: {
      title: {
        text: "Y Axis"
      },
      plotLines: [
        {
          value: mean,
          color: "green",
          dashStyle: "ShortDot",
          width: 2,
          label: {
            text: "Mean"
          }
        },
        /*{
          batch: "B0",
          value: 2.3124044515428785,
          lower_alert_limit: 2.12,
          upper_alert_limit: 3.2,
          lower_warning_limit: 2.15,
          upper_warning_limit: 3.3,
          lower_specification_limit: 2,
          upper_specification_limit: 3.4
        },*/
        {
          value: myData[0].lower_alert_limit,
          color: "red",
          dashStyle: "line",
          width: 2,
          label: {
            text: "lower_alert_limit"
          }
        },
        {
          value: myData[0].upper_alert_limit,
          color: "red",
          dashStyle: "line",
          width: 2,
          label: {
            text: "upper_alert_limit"
          }
        },
        {
          value: myData[0].lower_warning_limit,
          color: "orange",
          dashStyle: "longdash",
          width: 2,
          label: {
            text: "lower_warning_limit"
          }
        },
        {
          value: myData[0].upper_warning_limit,
          color: "orange",
          dashStyle: "longdash",
          width: 2,
          label: {
            text: "upper_warning_limit"
          }
        },
        {
          value: myData[0].lower_specification_limit,
          color: "blue",
          dashStyle: "shortdash",
          width: 2,
          label: {
            text: "lower_specification_limit"
          }
        },
        {
          value: myData[0].upper_specification_limit,
          color: "blue",
          dashStyle: "shortdash",
          width: 2,
          label: {
            text: "upper_specification_limit"
          }
        }
      ]
    },
    series: [
      {
        name: "DATA",
        color: "black",
        data: myRandomData
      }
    ],
    plotOptions: {
      series: {
        point: {
          events: {
            click: function() {
              cccc.series[0].addPoint(777);
            },
            mouseOver: function() {
              var chart = this.series.chart;
              if (!chart.lbl) {
                chart.lbl = chart.renderer
                  .label("")
                  .attr({
                    padding: 10,
                    r: 10,
                    fill: Highcharts.getOptions().colors[1]
                  })
                  .css({
                    color: "#FFFFFF"
                  })
                  .add();
              }
              chart.lbl.show().attr({
                text: "x: " + this.x + ", y: " + this.y
              });
              if (chart_) {
                if (chart_.series[0].data[this.x]) {
                  chart_.series[0].data[this.x].select(true);
                }
              }
            }
          }
        },
        events: {
          mouseOut: function() {
            if (this.chart.lbl) {
              this.chart.lbl.hide();
            }
          }
        }
      }
    }
  });
});
